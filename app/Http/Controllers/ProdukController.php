<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class ProdukController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list(Request $request)
    {
      
      $array= ['produk1','produk2','produk3','produk4','produk5','produk6','produk7','produk8','produk9'];
      $array2= ['Slider1','Slider2','Slider3','Slider4','Slider5','Slider6','Slider7','Slider8','Slider9'];
      $array3= ['Buku1','Buku2','Buku3','Buku4','Buku5','Buku6','Buku7','Buku8','Buku9'];

      

      return response()->json(['message' => 'Data added successfully', 'data' => ['produk' => $array,'slider' => $array2,'buku' => $array3 ]], 201);
    }
   
}
